package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
)

type Greeting struct {
	Text string `json: "text"`
	Team string `json: "team"`
}

var messageSlice = []Greeting{
	Greeting{Text: "Welcome", Team: "Web-Clan"},
	Greeting{Text: "Welcome", Team: "Mugun-Clan"},
}

func messageHandler(w http.ResponseWriter, r *http.Request) {
	enc := json.NewEncoder(w)
	enc.SetIndent(" ", "\t")
	enc.Encode(messageSlice)
}

func main() {
	var host = flag.String("addr", ":7000", "http service address of the application.")
	flag.Parse()
	srv := http.Server{Addr: ":7000"}
	go func() {
		<-time.After(9 * time.Second)

		fmt.Println("closing server in another thread")
		srv.Close()
		os.Exit(0)
	}()

	http.HandleFunc("/message", messageHandler)

	log.Println("Starting web server on ", *host)
	fmt.Println("Open browser and redirect to http://localhost:7000")

	if err := http.ListenAndServe(*host, nil); err != nil {
		log.Fatal("ListenAndServe:", err)
	}
}
